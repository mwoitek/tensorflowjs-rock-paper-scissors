const minSamples = 10;
const numEpochs = 10;

const dataset = new RPSDataset();
const webcam = new Webcam(document.getElementById('wc'));

let mobilenet;
let model;

let finalResults = {};
let isPredicting = false;

const classesButtons = document
  .getElementById('classesButtons')
  .querySelectorAll('button');
const classes = Array.from(classesButtons).map((button) => button.innerText);
const sampleCounts = Object.fromEntries(classes.map((class_) => [class_, 0]));

const buttonTrain = document.getElementById('buttonTrain');
const buttonDownload = document.getElementById('buttonDownload');

const trainMessage = document.getElementById('trainMessage');
const tableContainer = document.getElementById('tableContainer');
const resultsTable = document.getElementById('resultsTable');

const buttonStart = document.getElementById('buttonStart');
const buttonStop = document.getElementById('buttonStop');

const predictMessage = document.getElementById('predictMessage');
const predictionText = document.getElementById('predictionText');
const predictionImage = document.getElementById('predictionImage');

const createCountsTable = () => {
  const table = document.createElement('table');
  document.getElementById('sampleCounts').appendChild(table);

  for (const [class_, count] of Object.entries(sampleCounts)) {
    const tr = document.createElement('tr');
    table.appendChild(tr);

    const tdClass = document.createElement('td');
    tdClass.innerText = class_;
    tr.appendChild(tdClass);

    const tdCount = document.createElement('td');
    tdCount.classList.add('count');
    tdCount.id = `samples${class_}`;
    tdCount.innerText = count;
    tr.appendChild(tdCount);
  }
};

const enableTraining = () => {
  const canTrain = Object.values(sampleCounts).every(
    (count) => count >= minSamples
  );
  if (canTrain) {
    buttonTrain.removeAttribute('disabled');
    trainMessage.innerText = 'Click "Train" to train the model.';
  }
};

const handleButton = (event) => {
  const class_ = event.target.innerText;

  const tdCount = document.getElementById(`samples${class_}`);
  tdCount.innerText = ++sampleCounts[class_];

  enableTraining();

  const img = webcam.capture();
  const label = classes.indexOf(class_);
  dataset.addExample(mobilenet.predict(img), label);
};

classesButtons.forEach((button) =>
  button.addEventListener('click', handleButton)
);

const loadMobilenet = async () => {
  const mobilenet = await tf.loadLayersModel(
    'https://storage.googleapis.com/tfjs-models/tfjs/mobilenet_v1_1.0_224/model.json'
  );
  const layer = mobilenet.getLayer('conv_pw_13_relu');
  return tf.model({
    inputs: mobilenet.inputs,
    outputs: layer.output,
  });
};

const onEpochEnd = (epoch, logs) => {
  finalResults = Object.assign(finalResults, logs);

  const tr = document.createElement('tr');
  resultsTable.appendChild(tr);

  const tdEpoch = document.createElement('td');
  tdEpoch.innerText = epoch + 1;
  tr.appendChild(tdEpoch);

  const tdLoss = document.createElement('td');
  tdLoss.innerText = logs.loss.toFixed(5);
  tr.appendChild(tdLoss);

  const tdAccuracy = document.createElement('td');
  tdAccuracy.innerText = logs.acc.toFixed(3);
  tr.appendChild(tdAccuracy);

  tableContainer.removeAttribute('hidden');
};

const train = async () => {
  dataset.ys = null;

  const numClasses = classes.length;
  dataset.encodeLabels(numClasses);

  model = tf.sequential({
    layers: [
      tf.layers.flatten({
        inputShape: mobilenet.outputs[0].shape.slice(1),
      }),
      tf.layers.dense({
        units: 100,
        activation: 'relu',
      }),
      tf.layers.dense({
        units: numClasses,
        activation: 'softmax',
      }),
    ],
  });

  model.compile({
    optimizer: tf.train.adam(0.0001),
    loss: 'categoricalCrossentropy',
    metrics: ['accuracy'],
  });

  await model.fit(dataset.xs, dataset.ys, {
    epochs: numEpochs,
    callbacks: {
      onEpochEnd,
      onTrainEnd: () => {
        const tr = document.createElement('tr');
        document.getElementById('finalResultsTable').appendChild(tr);

        const tdLoss = document.createElement('td');
        tdLoss.innerText = finalResults.loss.toFixed(5);
        tr.appendChild(tdLoss);

        const tdAccuracy = document.createElement('td');
        tdAccuracy.innerText = finalResults.acc.toFixed(3);
        tr.appendChild(tdAccuracy);

        document.getElementById('finalResults').removeAttribute('hidden');
      },
    },
  });
};

buttonTrain.addEventListener('click', async () => {
  buttonTrain.setAttribute('disabled', '');
  trainMessage.innerText = 'Training model... This may take a while.';

  await train();

  trainMessage.innerText = 'Training done!';
  buttonDownload.removeAttribute('disabled');

  buttonStart.removeAttribute('disabled');
  predictMessage.innerText = 'Click "Start" to make predictions.';
});

buttonDownload.addEventListener('click', async () => {
  await model.save('downloads://my_model');
});

const updatePrediction = (classId) => {
  const class_ = classes[classId];
  predictionText.innerText = class_;
  predictionImage.src = `img/${class_}.png`;
};

const predict = async () => {
  while (isPredicting) {
    const predictedClass = tf.tidy(() => {
      const img = webcam.capture();
      const input = mobilenet.predict(img);
      const predictions = model.predict(input);
      return predictions.squeeze().argMax();
    });
    const classId = (await predictedClass.data())[0];
    updatePrediction(classId);
    predictedClass.dispose();
    await tf.nextFrame();
  }
};

buttonStart.addEventListener('click', async () => {
  buttonStart.setAttribute('disabled', '');
  buttonStop.removeAttribute('disabled');

  predictMessage.innerText =
    'Click "Stop" when you\'re done making predictions.';
  predictionText.removeAttribute('hidden');
  predictionImage.removeAttribute('hidden');

  isPredicting = true;
  await predict();
});

buttonStop.addEventListener('click', () => {
  isPredicting = false;

  buttonStart.removeAttribute('disabled');
  buttonStop.setAttribute('disabled', '');

  predictMessage.innerText = 'Click "Start" to make predictions.';
  predictionText.setAttribute('hidden', '');
  predictionImage.setAttribute('hidden', '');
});

(async () => {
  createCountsTable();
  trainMessage.innerText =
    'To begin training, ' +
    `collect at least ${minSamples} samples for each class.`;

  await webcam.setup();
  mobilenet = await loadMobilenet();
  tf.tidy(() => mobilenet.predict(webcam.capture()));
})();
